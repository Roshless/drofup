package com.roshless.drofup.adapters

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.roshless.drofup.R
import com.roshless.drofup.database.Paste
import com.roshless.drofup.database.PasteDatabase
import com.roshless.drofup.network.Requests

class PasteListAdapter(private var dataset: MutableList<Paste>) :
    RecyclerView.Adapter<PasteListAdapter.PasteViewHolder>() {

    class PasteViewHolder(val inputView: View) : RecyclerView.ViewHolder(inputView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PasteViewHolder {
        val textView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_entry, parent, false) as View

        return PasteViewHolder(textView)
    }

    override fun onBindViewHolder(holder: PasteViewHolder, position: Int) {
        val textViewName = holder.inputView.findViewById<TextView>(R.id.entry_on_list)
        textViewName.text = dataset[position].name
        val textViewDate = holder.inputView.findViewById<TextView>(R.id.date_on_list)
        textViewDate.text = dataset[position].date.toString()

        holder.inputView.setOnClickListener {
            val prefs = PreferenceManager.getDefaultSharedPreferences(it.context)
            val url = prefs.getString(it.context.getString(R.string.sharedprefs_server_url), "")
                .plus("/view/")
                .plus(textViewName.text)
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(it.context, intent, null)
        }

        holder.inputView.setOnLongClickListener {
            val builder = AlertDialog.Builder(it.context)
            LayoutInflater.from(it.context)

            val name = textViewName.text.toString()

            builder.apply {
                setTitle("Delete $name ?")
                setPositiveButton(R.string.yes) { _, _ ->
                    val request = Requests(it.context)
                    if (request.deleteFile(name)) {
                        val db = Room.databaseBuilder(
                            it.context,
                            PasteDatabase::class.java, "pastes"
                        ).allowMainThreadQueries().build()

                        val pasteToDelete = db.pasteDao().findByName(name)
                        db.pasteDao().delete(pasteToDelete)
                        db.close()

                        dataset.removeAt(position)
                        notifyItemRemoved(position)
                    }
                }
                setNegativeButton(R.string.no) { dialog, _ -> dialog.dismiss() }
            }

            builder.show()
            true
        }
    }

    override fun getItemCount() = dataset.size
}