package com.roshless.drofup.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [Paste::class], version = 1)
@TypeConverters(PasteConverters::class)
abstract class PasteDatabase : RoomDatabase() {
    abstract fun pasteDao(): PasteDao
}