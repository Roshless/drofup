package com.roshless.drofup.database

import androidx.room.TypeConverter
import org.threeten.bp.LocalDate

class PasteConverters {
    @TypeConverter
    fun dateToString(date: LocalDate): String {
        return date.toString()
    }

    @TypeConverter
    fun stringToDate(date: String): LocalDate {
        return LocalDate.parse(date)
    }
}