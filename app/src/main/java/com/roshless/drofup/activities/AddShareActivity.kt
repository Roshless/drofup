package com.roshless.drofup.activities

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.view.Gravity
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ShareCompat
import androidx.room.Room
import com.jakewharton.threetenabp.AndroidThreeTen
import com.roshless.drofup.R
import com.roshless.drofup.database.Paste
import com.roshless.drofup.database.PasteDatabase
import com.roshless.drofup.helpers.PasteListChanged
import com.roshless.drofup.network.Requests
import kotlinx.coroutines.*
import org.threeten.bp.LocalDate
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.util.UUID
import kotlin.coroutines.CoroutineContext

class AddShareActivity : Activity(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main
    private lateinit var mJob: Job
    private lateinit var textView: TextView
    private lateinit var buttonShare: Button
    private lateinit var buttonClipboard: Button
    private lateinit var pasteURL: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidThreeTen.init(this)
        setContentView(R.layout.share_add_paste)

        // coroutines
        mJob = Job()
        textView = findViewById(R.id.new_shared_paste_textview)
        buttonShare = findViewById(R.id.button_share_paste)
        buttonClipboard = findViewById(R.id.button_clipboard_paste)

        when (intent?.action) {
            Intent.ACTION_SEND -> {
                handleSendFile(intent)
                PasteListChanged.setTrue()
            }
        }

        buttonClipboard.setOnClickListener {
            if (!this::pasteURL.isInitialized)
                return@setOnClickListener

            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("paste", pasteURL)
            clipboard.setPrimaryClip(clip)

            Toast.makeText(this, "Link copied to clipboard", Toast.LENGTH_LONG).show()
        }

        buttonShare.setOnClickListener {
            if (!this::pasteURL.isInitialized)
                return@setOnClickListener

            ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setChooserTitle("Share paste URL")
                .setText(pasteURL)
                .startChooser()
        }
    }

    // coroutines
    override fun onDestroy() {
        super.onDestroy()
        mJob.cancel()
    }

    private fun handleSendFile(intent: Intent) {
        (intent.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as Uri).let {
            launch {
                val deferred = async(Dispatchers.Default) {
                    val request = Requests(applicationContext)
                    //   val bytes = FileInputStream(File(it.path)).use { input -> input.readBytes() }
                    withContext(Dispatchers.IO) {
                        request.postFile(readBytesFromUri(it))
                    }
                }

                val returnMsg = deferred.await()
                if (returnMsg.isNullOrBlank()) {
                    textView.text = getString(R.string.paste_send_failed)
                    textView.gravity = Gravity.CENTER
                    return@launch
                }
                textView.text = returnMsg
                textView.gravity = Gravity.CENTER
                pasteURL = returnMsg.trim()
                saveToDatabase(pasteURL)
            }
        }
    }

    // reading files into memory WHY NOT
    @Throws(IOException::class)
    private fun readBytesFromUri(uri: Uri): ByteArray {
        lateinit var output: ByteArray
        contentResolver.openInputStream(uri)?.use { inputStream ->
            output = inputStream.readBytes()
        }
        return output
    }

    private fun saveToDatabase(url: String) {
        val db = Room.databaseBuilder(
            applicationContext,
            PasteDatabase::class.java, "pastes"
        ).allowMainThreadQueries().build()

        val uuid = url.substringAfter("/view/")
        db.pasteDao().insertAll(Paste(0, UUID.fromString(uuid).toString(), LocalDate.now()))
        db.close()
    }
}