package com.roshless.drofup.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.jakewharton.threetenabp.AndroidThreeTen
import com.roshless.drofup.R
import com.roshless.drofup.adapters.PasteListAdapter
import com.roshless.drofup.helpers.PasteListChanged
import com.roshless.drofup.database.Paste
import com.roshless.drofup.database.PasteDatabase

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<PasteListAdapter.PasteViewHolder>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var pasteList: MutableList<Paste>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidThreeTen.init(this)
        setContentView(R.layout.list_activity)

        val db = Room.databaseBuilder(
            applicationContext,
            PasteDatabase::class.java, "pastes"
        ).allowMainThreadQueries().build()
        pasteList = db.pasteDao().getAll().toMutableList()
        db.close()

        viewManager = LinearLayoutManager(this)
        viewAdapter = PasteListAdapter(pasteList)

        recyclerView = findViewById<RecyclerView>(R.id.main_recycler_view).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                0
            )
        }
    }

    public override fun onResume() {
        super.onResume()
        if (PasteListChanged.hasChanged()) {
            val db = Room.databaseBuilder(
                applicationContext,
                PasteDatabase::class.java, "pastes"
            ).allowMainThreadQueries().build()
            val newList: List<Paste> = db.pasteDao().getAll().toMutableList()
            db.close()

            pasteList.clear()
            pasteList.addAll(newList)
            viewAdapter.notifyDataSetChanged()

            PasteListChanged.reset()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                val intent = Intent(applicationContext, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        super.onOptionsItemSelected(item)
        return false
    }
}
