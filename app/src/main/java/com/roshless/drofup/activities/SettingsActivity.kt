package com.roshless.drofup.activities

import android.app.Activity
import android.os.Bundle
import androidx.preference.PreferenceManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.roshless.drofup.R

class SettingsActivity : Activity() {
    private lateinit var buttonSave: Button
    private lateinit var editTextServerURL: EditText
    private lateinit var editTextUsername: EditText
    private lateinit var editTextPassword: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings)

        val sharedPrefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        buttonSave = findViewById(R.id.button_save)
        editTextServerURL = findViewById(R.id.preferences_serverurl)
        editTextUsername = findViewById(R.id.preferences_username)
        editTextPassword = findViewById(R.id.preferences_password)

        val serverUrl = sharedPrefs.getString(getString(R.string.sharedprefs_server_url), "")
        editTextServerURL.setText(serverUrl, TextView.BufferType.EDITABLE)

        val username = sharedPrefs.getString(getString(R.string.sharedprefs_username), "")
        editTextUsername.setText(username, TextView.BufferType.EDITABLE)

        val password = sharedPrefs.getString(getString(R.string.sharedprefs_password), "")
        editTextPassword.setText(password, TextView.BufferType.EDITABLE)

        buttonSave.setOnClickListener {
            with(sharedPrefs.edit()) {
                putString(
                    getString(R.string.sharedprefs_server_url),
                    editTextServerURL.text.toString()
                )
                putString(
                    getString(R.string.sharedprefs_username),
                    editTextUsername.text.toString()
                )
                putString(
                    getString(R.string.sharedprefs_password),
                    editTextPassword.text.toString()
                )
                apply()
            }
            Toast.makeText(applicationContext, "Preferences saved", Toast.LENGTH_SHORT).show()
            finish()
        }
    }
}