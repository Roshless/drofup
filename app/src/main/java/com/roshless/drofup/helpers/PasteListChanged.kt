package com.roshless.drofup.helpers

object PasteListChanged {
    private var hasChanged: Boolean = false

    fun hasChanged(): Boolean {
        return hasChanged
    }

    fun setTrue() {
        hasChanged = true
    }

    fun reset() {
        hasChanged = false
    }
}